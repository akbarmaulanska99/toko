<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Product;
use App\Member;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::with('categoryRef','categRef')->get();
        // $transactions = Transaction::with('categoryRef', 'categRef')->get();
        return view('transactions.index', compact('transactions'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $transactions = Transaction::all();
        $products = Product::all();
        $members = Member::all();
        return view('transactions.create', compact('products', 'members'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $trx_number = str_replace("-","",date("Y-m-d")).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUFWXYZ'),1,3);
        $price = Product::where('id',$request->product)->first()->price;
        $discountrate = Member::where('id', $request->members)->with('categoryRef')->first()->categoryRef->discount;
        $discount = $price * $request->quantity * $discountrate / 100;
        $total = $price * $request->quantity - $discount;

        $transactions = new Transaction;
        $transactions->trx_number = $trx_number;
        $transactions->product_id = $request->product;
        $transactions->member_id = $request->members;
        $transactions->quantity = $request->quantity;
        $transactions->discount = $discount;
        $transactions->total = $total;
        $transactions->save();
        
        return redirect()->route('transactions.index');    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $products = Product::all();
        $members = Member::all();
        return view('transactions.edit', compact('members', 'products', 'transaction'));   
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        $trx_number = str_replace("-","",date("Y-m-d")).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUFWXYZ'),1,3);
        $price = Product::where('id',$request->product)->first()->price;
        $discountrate = Member::where('id', $request->members)->with('categoryRef')->first()->categoryRef->discount;
        $discount = $price * $request->quantity * $discountrate / 100;
        $total = $price * $request->quantity - $discount;

        $transaction->trx_number = $trx_number;
        $transaction->product_id = $request->product;
        $transaction->member_id = $request->members;
        $transaction->quantity = $request->quantity;
        $transaction->discount = $discount;
        $transaction->total = $total;
        $transaction->update();
        
        return redirect()->route('transactions.index');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();
        return redirect()->route('transactions.index');
    }
}
