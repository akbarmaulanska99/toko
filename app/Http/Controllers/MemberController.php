<?php

namespace App\Http\Controllers;

use App\Member;
use App\MemberCategory;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Member::with('categoryRef')->get();
        return view('members.index', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $category = MemberCategory::all();
        return view('members.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $members = new Member;
        $members->member_category_id = $request->category;
        $members->full_name = $request->full_name;
        $members->dob = $request->dob;
        $members->address = $request->address;
        $members->gender = $request->gender;
        $barcode = str_replace("-","",$request->dob).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $members->barcode = $barcode;
        $members->save();
        return redirect()->route('members.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::with('pembelian')->findOrfail($id);
        return view('members.history', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        $category = MemberCategory::all();
        return view('members.edit', compact('category','member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
        $member->member_category_id = $request->category;
        $member->full_name = $request->full_name;
        $member->dob = $request->dob;
        $member->address = $request->address;
        $member->gender = $request->gender;
        $barcode = str_replace("", "", $request->date_of_birth).substr(str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
        $member->barcode = $barcode;
        $member->update();
        return redirect()->route('members.index');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();
        return redirect()->route('members.index');
    }
}
