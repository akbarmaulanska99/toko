<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public function categoryRef()
    {
    	return $this->belongsTo(MemberCategory::class,'member_category_id');
    }
    public function pembelian()
    {
    	return $this->hasMany(Transaction::class,);
    }
}
