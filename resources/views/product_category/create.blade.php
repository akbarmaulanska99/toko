@extends('layouts.template')

@section('content')

<div class="card">
  <div class="card-header">
   <h4> Tambah Kategori Produk</h4>
  </div>
  <div class="card-body">
    <form action="{{route('product_category.store') }}" method="post">
            @csrf
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control" 
                            id="name" placeholder="Masukkan Nama" required>
                        </div>
                        <div class="form-group">
                            <label for="desc">Deskripsi</label>
                            <textarea class="form-control" id="desc" name="desc" 
                             rows="4" required></textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-danger">Tambah</button>
        </form>
    </div>
  </div>
</div>
@endsection




