@extends('layouts.template')

@section('content')

<div class="card">
  <div class="card-header">
   <h4> Ubah Kategori Produk</h4>
  </div>
  <div class="card-body">
    <form action="{{route('product_category.update', $category->id) }}" method="post">
            @csrf
            @method('put')
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="name">Nama</label>
                            <input type="text" name="name" class="form-control" 
                            id="name" value="{{ $category->name }}">
                        </div>
                        <div class="form-group">
                            <label for="desc">Deskripsi</label>
                            <textarea class="form-control" id="desc" name="desc" 
                             rows="4" required value="{{ $category->desc }}"></textarea>
                             <!-- <input type="text" name="desc" class="form-control" 
                            id="desc" value="{{ $category->desc }}"> -->
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Simpan</button>
        </form>
    </div>
  </div>
</div>
@endsection




