 @extends('layouts.template')

@section('content')

 <div class="col-lg-12 stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h2 class="title-12 m-b-0">Kategori Produk</h2>

                    <div class="table-data__tool">
                        <div class="table-data__tool-left">
                        </div>
                        <div class="table-data__tool-right">
                            
                            <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                               <a href="{{ route('product_category.create') }}">
                            <button class="au-btn au-btn-icon au-btn--blue au-btn--small">Tambah</button></a>
                                <div class="dropDownSelect2"></div>
                            </div>
                        </div>
                     </div>
                 
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th> No </th>
                          <th> Nama </th>
                          <th> Deskripsi </th>
                          
                         
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($category as $i => $item)
                        <tr class="table-info">
                          <td>{{$i+1}}</td>
                          <td>{{$item->name}}</td>
                          <td>{{$item->desc}}</td>
                            <td>
                              <div class="table-data-feature">
                               <form action="{{ route('product_category.destroy',$item->id) }}" method="post">

                                <a href="{{ route('product_category.edit',$item->id) }}">
                                <button class="btn btn-success" type="submit" title="Edit">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                                </a>
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger"  title="Delete"
                                onclick="return comfirm('yakin?')">
                                    <i class="zmdi zmdi-delete"></i>
                                </button>
                                </form>
                              </div>
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

              @endsection 