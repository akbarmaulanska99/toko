@extends('layouts.template')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-12 m-b-30">Data Produk</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">All Properties</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <a href="{{ route ('products.create') }}">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">Tambah</button></a>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div>
        </div>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        
                        <th>No</th>
                        <th>nama</th>
                        <th>gambar</th>
                        <th>kategori</th>
                        <th>deskripsi</th>
                        <th>harga</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $i => $item)
                    <tr class="tr-shadow">
                     
                        <td>{{$i+1}}</td>
                        <td>{{$item->name}}</td>
                        <td><img src="{{ URL::to('/') }}/uploads/products/{{$item->image}}" class="img-thumbnail" width="100px" height="100px"></td>
                        <td>{{$item->categoryRef->name}}</td>
                        <td>{{$item->desc}}</td>
                        <td>Rp.{{$item->price}}</td>
                        <td>
                            <div class="table-data-feature">
                               <form action="{{ route ('products.destroy',$item->id) }}" method="post">
                                <a href="{{ route('products.edit',$item->id) }}">
                                    <button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="zmdi zmdi-edit"></i>
                                    </button>
                                    </a>
                                   
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger" type="submit" title="Delete">
                                        <i class="zmdi zmdi-delete"></i>
                                </button>
                                 <a href="{{ route('products.show',$item->id) }}">
                                    <button class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="detail">
                                        <i>{detail} </i>
                                    </button>
                                    </a>
                               </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>

@endsection