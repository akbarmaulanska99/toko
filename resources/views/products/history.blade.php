 @extends('layouts.template')

@section('content')

 <div class="col-lg-12 stretch-card">
    <div class="card">
        <h4 class="title-12 m-b-0"></h4>
      <div class="card-body">
        <div class="table-data__tool">
            <div class="table-data__tool-left">
            </div>
            <div class="table-data__tool-right">
            </div>
         </div>
     
        <table class="table table-bordered">
          <thead>
            <tr>
              <th> No </th>
              <th> Kode Transaksi </th>
              <th> Nama Lengkap </th>
              <th> Jumlah </th>
              <th> Diskon </th>
              <th> Total </th>
              
             
            </tr>
          </thead>
          <tbody>
            @foreach($products->pembelian as $i => $item)
            <tr class="table-info">
              <td>{{$i+1}}</td>
              <td>{{$item->trx_number}}</td>
              <td>{{$item->categRef->full_name}}</td>
              <td>{{$item->quantity}}</td>
              <td>{{$item->discount}}</td>
              <td>{{$item->total}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @endsection 