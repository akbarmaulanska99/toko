@extends('layouts.template')

@section('content')

<div class="card">
  <div class="card-header">
   <h4> Tambah Kategori Produk</h4>
  </div>
  <div class="card-body">
    <form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
        @csrf

    <div class="form-group col-md-12">
            <label for="name">Nama Produk</label>
            <input type="name" name="name" class="form-control"
             id="title" placeholder="Masukan Nama Produk">
        </div>
    
    <div class="form-group col-md-12">
        <label>Kategori Produk</label>
            <select class="form-control show-tick" id="category" name="category">
                <option>chose...</option>
                @foreach ($category as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
    </div>

    <div class="form-group col-md-6">
        <label for="image">Gambar</label>
        <input type="file" class="form-control" id="image" 
        name="image" placeholder="Masukan Deskripsi">
    </div>
    <div class="form-group col-md-6">
        <label for="price">Harga</label>
        <input type="number" class="form-control" id="price" 
        name="price" placeholder="Masukan Harga">
    </div>
    <div class="form-group col-md-6">
        <label for="desc">Deskripsi</label>
        <input type="text" class="form-control" id="desc" 
        name="desc" placeholder="Masukan Deskripsi">
    </div>
    
        <button type="submit" class="btn btn-success">Simpan</button>
    </form>
    </div>
  </div>
</div>
@endsection




