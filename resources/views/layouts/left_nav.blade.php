
        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="/home">
                    <img src="{{ asset ('template/images/icon/logo.png') }}" width="180" height="140" style="padding-left: 50px;" alt="" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                         <li>
                            <a href="/home">
                                <i class="fas fa-home"></i>Home</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-tags"></i>Produk</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ route ('products.index') }}">Data Produk</a>
                                </li>
                                <li>
                                    <a href="{{ route ('product_category.index') }}">Kategori Produk</a>
                                </li>
                              
                            </ul>
                        </li>
                       
                       <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-user"></i>Member</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{ route ('members.index') }}">Data Member</a>
                                </li>
                                <li>
                                    <a href="{{ route ('member_category.index') }}">Kategori Member</a>
                                </li>
                                </ul>
                         <li>
                            <a href="">
                                <i class="far fa-check-square"></i>Promo</a>
                        </li>
                        <li>
                            <a href="{{ route('transactions.index') }}">
                                <i class="fas fa-shopping-cart"></i>Transaksi</a>
                        </li>
                    
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->