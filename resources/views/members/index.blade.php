@extends('layouts.template')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- DATA TABLE -->
        <h3 class="title-12 m-b-30">Data Member</h3>
        <div class="table-data__tool">
            <div class="table-data__tool-left">
                <div class="rs-select2--light rs-select2--md">
                    <select class="js-select2" name="property">
                        <option selected="selected">All Properties</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
                
                <button class="au-btn-filter">
                    <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">
                <a href="{{ route('members.create') }}">
                <button class="au-btn au-btn-icon au-btn--green au-btn--small">Tambah</button></a>
                <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                    <select class="js-select2" name="type">
                        <option selected="selected">Export</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                    </select>
                    <div class="dropDownSelect2"></div>
                </div>
            </div>
        </div>
        <div class="table-responsive table-responsive-data2">
            <table class="table table-data2">
                <thead>
                    <tr>
                        
                        <th>No</th>
                        <th>Kategori Anggota</th>
                        <th>Nama Lengkap</th>
                        <th>Tanggal Lahir</th>
                        <th>Alamat</th>
                        <th>Gender</th>
                        <th>Kode Member</th>

                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($members as $i => $item)
                    <tr class="tr-shadow">
                     
                        <td>{{$i+1}}.</td>
                        <td>{{$item->categoryRef->name}}</td>
                        <td>{{$item->full_name}}</td>
                        <td>{{$item->dob}}</td>
                        <td>{{$item->address}}</td>
                        <td>{{$item->gender}}</td>
                        <td>{{$item->barcode}}</td>
                        <td>
                            <div class="table-data-feature">
                               <form action="{{ route ('members.destroy',$item->id) }}" method="post">
                                <a href="{{ route('members.edit',$item->id) }}">
                                    <button class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i>edit</i>
                                    </button>
                                    </a>
                                    <a href="{{ route('members.show', $item->id) }}">
                                    <button type="button" class="btn btn-primary" title="Detail"> Detail</button></a>
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger" type="submit" title="Delete">
                                        <i>delete</i>
                                </button>
                                 
                               </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- END DATA TABLE -->
    </div>
</div>

@endsection