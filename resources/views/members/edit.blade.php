@extends('layouts.template')

@section('content')

<div class="card">
  <div class="card-header">
   <h4> Tambah Data Member</h4>
  </div>
  <div class="card-body">
    <form action="{{ route('members.update', $member->id) }}" method="post">
        @csrf
        @method('put')

    <div class="form-group col-md-12">
            <label for="name">Nama Lengkap</label>
            <input type="text" name="full_name" class="form-control"
             id="full_name" placeholder="Masukan Nama Produk">
        </div>
    
    <div class="form-group col-md-12">
        <label>Kategori Anggota</label>
            <select class="form-control show-tick" id="category" name="category">
                <option>chose...</option>
                @foreach ($category as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
    </div>

    <div class="form-group col-md-6">
        <label for="dob">Tanggal Lahir</label>
        <input type="date" class="form-control" id="dob" 
        name="dob">
    </div>
    <div class="form-group col-md-6">
        <label for="address">Alamat</label>
        <input type="text" class="form-control" id="address" 
        name="address" placeholder="Masukan Alamat">
    </div>
    <div class="form-group">
    <label>Gender</label>
    <div class="show-trick"></div>
        <input type="radio" name="gender" id="gender1" value="pria" class="with-gap"
        {{ $member->gender == 'pria' ? 'checked' : '' }}>
        <label for="gender1"> Pria </label>

         <input type="radio" name="gender" id="gender1" value="pria" class="with-gap"
         {{ $member->gender == 'wanita' ? 'checked' : '' }}>
        <label for="gender1"> Wanita </label>
    </div>
    
        <button type="submit" class="btn btn-success">Simpan</button>
    </form>
    </div>
  </div>
</div>
@endsection

