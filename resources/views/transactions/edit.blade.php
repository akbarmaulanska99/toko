@extends('layouts.template')

@section('content')

<div class="card">
  <div class="card-header">
   <h4> Tambah Data Member</h4>
  </div>
  <div class="card-body">
    <form action="{{ route('transactions.update',$transaction->id) }}" method="post">
        @csrf
        @method('put')

    <div class="form-group col-md-12">
            <label for="name">Nama Lengkap</label>
            <select class="form-control show-tick" id="members" name="members">
                <option>chose...</option>
                @foreach ($members as $item)
                <option value="{{ $item->id }}"{{ $transaction->member_id == $item->id ? 'selected' : ''}} >{{ $item->full_name }}</option>
                @endforeach
            </select>
        </div>
    
    <div class="form-group col-md-12">
        <label>Nama Produk</label>
            <select class="form-control show-tick" id="product" name="product">
                <option>chose...</option>
                @foreach ($products as $item)
                <option value="{{ $item->id }}" {{ $transaction->product_id == $item->id ? 'selected' : ''}} >{{ $item->name }}</option>
                @endforeach
            </select>
    </div>

    <div class="form-group col-md-6">
        <label for="quantity">Jumlah</label>
        <input type="number" class="form-control" id="quantity" 
        name="quantity" value="{{ $transaction->quantity }}">
    
    <div class="form-group">
    </div>
    
        <button type="submit" class="btn btn-success">Simpan</button>
    </form>
    </div>
  </div>
</div>
@endsection