@extends('layouts.template')

@section('content')

<div class="card">
  <div class="card-header">
   <h4> Tambah Kategori Member</h4>
  </div>
  <div class="card-body">
    <form action="{{ route('member_category.store') }}" method="post">
        @csrf
    
    <div class="form-group">
    <label for="name">Nama Kategori</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama">
  </div>
  <div class="form-group">
    <label for="name">Diskon</label>
    <input type="number" class="form-control" id="discount" name="discount" placeholder="Masukan Diskon">
  </div>
     <button type="submit" class="btn btn-danger">Tambah</button>
    </form>
  </div>
</div>
@endsection







