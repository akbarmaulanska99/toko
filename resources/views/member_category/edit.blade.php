@extends('layouts.template')

@section('content')

<div class="card">
  <div class="card-header">
   <h4> Tambah Kategori Member</h4>
  </div>
  <div class="card-body">
    <form action="{{ route('member_category.update', $category->id) }}" method="post">
        @csrf
        @method('put')
    
    <div class="form-group">
    <label for="name">Nama Kategori</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ $category->name }}">
  </div>
     <button type="submit" class="btn btn-danger">Ubah</button>
    </form>
  </div>
</div>
@endsection







